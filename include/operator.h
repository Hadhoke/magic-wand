// -*- C++ -*-
/*
     _               _ _           _        
    | |             | | |         | |       
    | |__   __ _  __| | |__   ___ | | _____ 
    | '_ \ / _` |/ _` | '_ \ / _ \| |/ / _ \
    | | | | (_| | (_| | | | | (_) |   <  __/
    |_| |_|\__,_|\__,_|_| |_|\___/|_|\_\___|

  operator.h - This file is part of the magic-wand project

*/

#ifndef magicwand_operator_h
#define magicwand_operator_h

#include <vector>

// Uncomment to use png input image
// (don't forget to uncomment last line of CMakeLists.txt to add libpng and libjpeg)
#define cimg_use_png 1
#define cimg_use_jpeg 1
  
#include "CImg.h"

typedef cimg_library::CImg<uint8_t> ImageUchar;
typedef std::vector<uint8_t> Pixel;

struct Point2D {
  int32_t x;
  int32_t y;
};

bool operator==(Point2D const& a, Point2D const& b);
bool operator!=(Point2D const& a, Point2D const& b);
void operator+=(Pixel& a, const Pixel& b);
void operator/=(Pixel& a, const Pixel& b);

// Constants to iterate neighbours
// 0 1 2
// 3 4 5
// 6 7 8
const int8_t dx9[9] = {-1,  0,  1, -1,  0,  1, -1,  0,  1};
const int8_t dy9[9] = {-1, -1, -1,  0,  0,  0,  1,  1,  1};

enum Event {KEY_A, KEY_R, KEY_Z, CLICK_LEFT, MOUSE_MOVE};

void magic_wand(const ImageUchar& img_src,
                      ImageUchar& mask,
                const std::vector<Point2D>& points,
                const uint8_t threshold);
bool is_valid_pixel(const Point2D& p, const ImageUchar& img, const ImageUchar& mask, const Pixel& pixel_ref, const uint8_t threshold);
void merge_mask(ImageUchar& img, const ImageUchar& mask);
void contour(ImageUchar& img);
void dilatation(ImageUchar& img);
void gauss_filtering(ImageUchar& img_src, const int halfsize);


void run(const std::string& image_name);
Event get_event(cimg_library::CImgDisplay& disp);
int key_pressed_to_delta(const Event& key);
void propagate_point(const ImageUchar& img_src, ImageUchar& mask,
                     const Point2D& point,      const uint8_t threshold);
void get_position(const cimg_library::CImgDisplay& disp, Point2D& point);
void update_display(cimg_library::CImgDisplay& disp, const ImageUchar& img,
                    const ImageUchar& mask);
void get_pixel_value(const ImageUchar& img, const Point2D& p, Pixel& pixel);
void neighbor_pixel_average(const Point2D& p, const ImageUchar& img, Pixel& pixel,
                            const int connexity);


#endif
