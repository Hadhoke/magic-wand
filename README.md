
# Installation
--------------

## Outils requis

Pour compiler, vous avez besoin de CMake et de la librairie X11-dev.

#### Linux

    $ sudo apt-get install libx11-dev
    $ sudo apt-get install cmake

#### Mac OS
Téléchargez XQuartz (X11) ici : [http://xquartz.macosforge.org/](http://xquartz.macosforge.org/)

Si vous n'avez pas homebrew, téléchargez le ici : [http://brew.sh](http://brew.sh) (en bas de la page), puis

    $ brew install cmake

## Compilation

    $ git clone https://bitbucket.org/Hadhoke/magic-wand.git
    $ cd magic-wand
    $ mkdir build && cd $_
    $ cmake .. -B.
    $ make

L'exécutable est créé dans le répertoire `build/`

# Utilisation
---------------------

    $ magic-wand  <image_path>

Premier clique : rentre en mode draw-line
Second clique : quitte le mode draw-line

A : augmente le seuil
Z : diminue le seuil
R : reset
