// -*- C++ -*-
/*
     _               _ _           _        
    | |             | | |         | |       
    | |__   __ _  __| | |__   ___ | | _____ 
    | '_ \ / _` |/ _` | '_ \ / _ \| |/ / _ \
    | | | | (_| | (_| | | | | (_) |   <  __/
    |_| |_|\__,_|\__,_|_| |_|\___/|_|\_\___|

  operator.cpp - This file is part of the magic-wand project

*/

#include <iostream>
#include <queue>
#include <vector>
#include <math.h>

#include "operator.h"

bool operator==(Point2D const& a, Point2D const& b) {
  return (a.x == b.x && a.y == b.y);
}

bool operator!=(Point2D const& a, Point2D const& b) {
  return !(a == b);
}

void operator+=(Pixel& a, const Pixel& b) {
  for(int i = 0; i < int(a.size()); ++i) {
    a[i] += b[i];
  }
}

void operator/=(Pixel& a, const int& divisor) {
  for(auto& p : a) {
    p /= divisor;
  }
}

void run(const std::string &image_name) {
  ImageUchar img_src  (image_name.c_str());
  ImageUchar img_gauss(img_src);
  ImageUchar mask     (img_src.width(), img_src.height());
  uint8_t threshold = 10;

  cimg_library::CImgDisplay main_disp(img_src, "Click on the image");

  gauss_filtering(img_gauss, 3);

  std::vector<Point2D> points;

  while (! main_disp.is_closed()) {
    Event event = get_event(main_disp);
    if (event == KEY_R) {
      points.clear();
      main_disp.display(img_src);
      mask = uint8_t(0);
      std::cout << "Clear magic wand " << points.size() << std::endl;
      continue;
    } else if (event != CLICK_LEFT) {
      continue;
    }

    std::cout << "Enter draw line mode" << std::endl;

    Point2D p;
    get_position(main_disp, p);
    points.push_back(p);
    bool point_added = true;

    while (true) {
      if (point_added) {
        mask = uint8_t(0);
        magic_wand(img_gauss, mask, points, threshold);
        point_added = false;
      }

      update_display(main_disp, img_src, mask);

      event = get_event(main_disp);

      if (event == CLICK_LEFT) {
        break;
      } else if (event == MOUSE_MOVE) {
        get_position(main_disp, p);
        if (p != points.back()) {
          points.push_back(p);
          point_added = true;
          std::cout << "Add point (" << p.x << ", " << p.y << ")" << std::endl;
        }
      } else if (event == KEY_A || event == KEY_Z) {
        threshold += key_pressed_to_delta(event);
        std::cout << "Update threshold (" << int(threshold) << ")" << std::endl;
        point_added = true;
      }
    }
    std::cout << "Quit draw line mode" << std::endl;
  }
}

void update_display(cimg_library::CImgDisplay& disp, const ImageUchar& img, const ImageUchar& mask) {
  ImageUchar img_tmp(img);
  merge_mask(img_tmp, mask);
  disp.display(img_tmp);
}

Event get_event(cimg_library::CImgDisplay& disp) {
  disp.wait();
  if      (disp.is_keyA())    { return KEY_A; }
  else if (disp.is_keyZ())    { return KEY_Z; }
  else if (disp.is_keyR())    { return KEY_R; }
  else if (disp.button() & 1) { return CLICK_LEFT; }
  else                        { return MOUSE_MOVE;  }
}

int key_pressed_to_delta(const Event& key) {
  switch (key) {
    case KEY_A:
      return 1;
    case KEY_Z:
      return -1;
    default:
      return 0;
  }
}

void magic_wand(const ImageUchar& img_src,
                      ImageUchar& mask,
                const std::vector<Point2D>& points,
                const uint8_t threshold) {
  std::cout << points.size() << " points to compute ..." << std::flush;
  for (const Point2D& p : points) {
    ImageUchar mask_tmp(img_src.width(), img_src.height());
    propagate_point(img_src, mask_tmp, p, threshold);
    mask |= mask_tmp;
  }
  dilatation(mask);
  dilatation(mask);
  contour(mask);
  std::cout << " COMPLETED" << std::endl;
}

void propagate_point(const ImageUchar& img_src, ImageUchar& mask,
                     const Point2D& point,      const uint8_t threshold) {
  mask = uint8_t(0);

  Pixel pixel_ref(img_src.spectrum());
  neighbor_pixel_average(point, img_src, pixel_ref, 8);

  std::queue<Point2D> queue;
  queue.push(point);

  while (! queue.empty()) {
    Point2D p_current = queue.front();
    queue.pop();
    get_pixel_value(img_src, p_current, pixel_ref);
    for (int i = 0; i < 9; ++i) {
      const Point2D neighbor { p_current.x + dx9[i],
                               p_current.y + dy9[i] };
      if (is_valid_pixel(neighbor, img_src, mask, pixel_ref, threshold)) {
        queue.push(neighbor);
        mask(neighbor.x, neighbor.y) = UINT8_MAX;
      }
    }
  }
}

bool is_valid_pixel(const Point2D& p, const ImageUchar& img, const ImageUchar& mask, const Pixel& pixel_ref, const uint8_t threshold) {
  if (! img.containsXYZC(p.x, p.y) || mask(p.x, p.y) == UINT8_MAX) {
    return false;
  }
  
  int distance = 0;

  for (int c = 0; c < img.spectrum(); ++c) {
    int diff = pixel_ref[c] - img(p.x, p.y, c);
    distance += diff * diff;
  }
  
  if (distance > threshold * threshold) {
    return false;
  }
  return true; 
}

void neighbor_pixel_average(const Point2D& p, const ImageUchar& img, Pixel& pixel,
                            const int connexity) {
  if (connexity == 8) {
    for (int i = 0; i < 9; ++i) {
      Pixel current_pixel(pixel.size());
      const Point2D neighbor { p.x + dx9[i],
                               p.y + dy9[i]};
      get_pixel_value(img, neighbor, current_pixel);
      pixel += current_pixel;
    }
    pixel /= 9;
  }
}

void get_pixel_value(const ImageUchar& img, const Point2D& p, Pixel& pixel) {
  for (int c = 0; c < img.spectrum(); ++c) {
    pixel[c] = img(p.x, p.y, c);
  }
}

void get_position(const cimg_library::CImgDisplay& disp, Point2D& point) {
  point.x = disp.mouse_x();
  point.y = disp.mouse_y();
}

void wait_click(cimg_library::CImgDisplay& disp) {
  while (! disp.is_closed()) {
    disp.wait();
    if (disp.button() && disp.mouse_y() >= 0) {
      return;
    }
  }
  return;
}

void merge_mask(ImageUchar& img, const ImageUchar& mask) {
  for (int y = 0; y < img.height(); ++y) {
    for (int x = 0; x < img.width(); ++x) {
      if (mask(x, y) == UINT8_MAX) {
        for (int c = 0; c < img.spectrum(); ++c) {
          img(x, y, c) = 0;
          img(x+1, y, c) = 0;
          img(x-1, y, c) = 0;
          img(x, y+1, c) = 0;
          img(x, y-1, c) = 0;
        }
      }
    }
  }
}

void contour(ImageUchar& img) {
  ImageUchar img_tmp(img.width(), img.height());
  img_tmp = uint8_t(0);
  for (int y = 1; y < img.height() - 1; ++y) {
    for (int x = 1; x < img.width() - 1; ++x) {
      if (img(x, y) == UINT8_MAX) {
        int neighbor_sum = 0;
        for (int i = 0; i < 9; ++i) {
          neighbor_sum += img(x + dx9[i], y + dy9[i]);
        }
        if(neighbor_sum != UINT8_MAX*9) {
          img_tmp(x, y) = UINT8_MAX;
        }
      }
    }
  }
  img = img_tmp;
}

/**
 * dilatation 8-connexe
 * Each pixel is set with the maximum value of his height neighbors and himself
 *
 * @param image [in, out] Grayscale image to dilate
 */
void dilatation(ImageUchar& image) {
  ImageUchar dilated_image(image.width(), image.height());

  for (int y = 0; y < image.height(); ++y) {
    for (int x = 0; x < image.width(); ++x) {
      uint8_t max = 0;
      for (int k = 0; k < 9; ++k) { // Loop on neighboring
        int x_tmp = x + dx9[k];
        int y_tmp = y + dy9[k];
        if (image.containsXYZC(x_tmp, y_tmp) && image(x_tmp, y_tmp) > max) {
          max = image(x_tmp, y_tmp);
        }
      }
      dilated_image(x, y) = max;
    }
  }
  image = dilated_image;
}

void gauss_filtering(ImageUchar& img_src, const int halfsize) {
  const int size = halfsize * 2 + 1;
  float se[size]; //structuring element
  
  ImageUchar img_tmp(img_src.width(), img_src.height());

  const float sigma = (float)(halfsize) / 3;
  
  for (int i = -halfsize; i <= halfsize; ++i) {
    se[i + halfsize] = exp(-(((i * i)) / (2 * sigma * sigma))) * (1 / (sqrt(2 * M_PI) * sigma));
  }

  for (int y = 0; y < img_src.height(); ++y) {
    for (int x = 0; x < img_src.width(); ++x) {
      float sum     = 0;
      float divisor = 0;
      for (int i = 0; i < size; ++i) {
        if(img_src.containsXYZC(x - halfsize + i, y)) {
          sum     += img_src(x - halfsize + i, y) * se[i];
          divisor += se[i];
        }
      }
      img_tmp(x, y) = sum / divisor;
    }
  }

  for (int y = 0; y < img_src.height(); ++y) {
    for (int x = 0; x < img_src.width(); ++x) {
      float sum     = 0;
      float divisor = 0;
      for (int i = 0; i < size; ++i) {
        if (img_src.containsXYZC(x, y - halfsize + i)) {
          sum     += img_tmp(x , y - halfsize + i) * se[i];
          divisor += se[i];
        }
      }
      img_src(x, y) = sum / divisor;
    }
  }
}
