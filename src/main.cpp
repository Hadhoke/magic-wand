// -*- C++ -*-
/*
     _               _ _           _        
    | |             | | |         | |       
    | |__   __ _  __| | |__   ___ | | _____ 
    | '_ \ / _` |/ _` | '_ \ / _ \| |/ / _ \
    | | | | (_| | (_| | | | | (_) |   <  __/
    |_| |_|\__,_|\__,_|_| |_|\___/|_|\_\___|

  main.cpp - This file is part of the magic-wand project

*/

#include <iostream>

#include "CImg.h"

#include "operator.h"
  
/**
 * Print help
 */
void show_help() {
  std::cout << "=== Magic Wand ===" << std::endl
            << "Usage: " << std::endl
            << "  magic-wand <image_name>" << std::endl;
}


int main(int argc, const char* argv[]) {
  if (argc != 2) {
    show_help();
    return 0;
  }

  run(argv[1]);
  
  return 0;
}

